<?php

// Set Variables
$LOCAL_ROOT         = "'C:/wamp/www/soap/Prometheus/'";
$LOCAL_REPO_NAME    = "Prometheus";
$LOCAL_REPO         = "{$LOCAL_ROOT}/{$LOCAL_REPO_NAME}";
$REMOTE_REPO        = "https://BrianNyatsine@bitbucket.org/cavalrymedia/prometheus.git";
$BRANCH             = "master";


$output = '';

// if ( $_POST['payload'] ) {
  // Only respond to POST requests from Github

  if( file_exists($LOCAL_REPO) ) {

    // If there is already a repo, just run a git pull to grab the latest changes
    $tmp = shell_exec("cd {$LOCAL_REPO} && git pull");
        
        $output .= "<span style=\"color: #6BE234;\">\$</span><span style=\"color: #729FCF;\">{$tmp}\n</span><br />";
        $output .= htmlentities(trim($tmp)) . "\n<br /><br />";

    // die("done " . mktime());
  } else {

    // If the repo does not exist, then clone it into the parent directory
    $tmp = shell_exec("cd {$LOCAL_ROOT} && git clone {$REMOTE_REPO}");

    $output .= "<span style=\"color: #6BE234;\">\$</span><span style=\"color: #729FCF;\">{$tmp}\n</span><br />";
        $output .= htmlentities(trim($tmp)) . "\n<br /><br />";

    // die("done " . mktime());
  }
// }

?>


<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title>GIT DEPLOYMENT SCRIPT</title>
</head>
<body style="background-color: #000000; color: #FFFFFF; font-weight: bold; padding: 0 10px;">
<div style="width:700px">
    <div style="float:left;width:350px;">
    <p style="color:white;">Git Deployment Script</p>
    <?php echo $output; ?>
    </div>
</div>
</body>
</html>