<?php
 
 class Prometheus
 {
  
  // our endpoint will look like:
  // http://domain.location/prometheus.php/function-name/other/options/etc
  //just a thing thing thang 3
  
  private static $allowed_methods = array('GET', 'POST', 'PUT', 'DELETE');
  
  private static $method  = '';
  private static $status_code = 0;
  
  public static function processRequest()
  {
   header("Access-Control-Allow-Origin: *");
   header("Access-Control-Allow-Methods: " . implode(', ', self::$allowed_methods));
   header("Content-Type: application/json");
   
   self::$method = $_SERVER['REQUEST_METHOD'];
   if (self::$method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER))
   {
    if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE')
     self::$method = 'DELETE';
    else if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT')
     self::$method = 'PUT';
    else
     self::$method = 'UNKNOWN';
   }
   
   $path_info = ltrim((isset($_SERVER['PATH_INFO']) ? trim($_SERVER['PATH_INFO']) : ''), '/');
   $path_info = explode('/', $path_info);
   $clean_info = array();
   foreach ($path_info as $item)
   {
    if (trim($item) != '')
     $clean_info[] = $item;
   }
   $path_info = $clean_info;
   
   print_r($path_info);
   print_r($_SERVER);
  }

  public function unzipFiles()
  {
    /**
     * Unzip File in the same directory.
     */
    $file = 'Deploy.zip';

    // get the absolute path to $file
    $path = pathinfo(realpath($file), PATHINFO_DIRNAME);

    $zip = new ZipArchive;
    $res = $zip->open($file);
    if ($res === TRUE) {
      // extract it to the path we determined above
      $zip->extractTo($path);
      $zip->close();
      return "Completed Extracting ".$file ."to".$path."";
    } else {
      return "Failed to extract ".$file ."to".$path."";
    }
  }

  public function zipFiles() 
  {
    set_time_limit(0);
    /**
     * ZIP All content of current folder
     */
 
    /* ZIP File name and path */
    $zip_file = 'Deploy.zip';
     
    /* Exclude Files */
    $exclude_files = array();
    $exclude_files[] = realpath( $zip_file );
    $exclude_files[] = realpath( 'Prometheus.php' );
     
    /* Path of current folder, need empty or null param for current folder */
    $root_path = realpath( '' );
     
    /* Initialize archive object */
    $zip = new ZipArchive;
    $zip_open = $zip->open( $zip_file, ZipArchive::CREATE );
     
    /* Create recursive files list */
    $files = new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator( $root_path ),
        RecursiveIteratorIterator::LEAVES_ONLY
    );
     
    /* For each files, get each path and add it in zip */
    if( !empty( $files ) ){
     
        foreach( $files as $name => $file ) {
     
            /* get path of the file */
            $file_path = $file->getRealPath();
     
            /* only if it's a file and not directory, and not excluded. */
            if( !is_dir( $file_path ) && !in_array( $file_path, $exclude_files ) ){
     
                /* get relative path */
                $file_relative_path = str_replace( $root_path, '', $file_path );
     
                /* Add file to zip archive */
                $zip_addfile = $zip->addFile( $file_path, $file_relative_path );
            }
        }
    }
     
    /* Create ZIP after closing the object. */
    $zip_close = $zip->close();
    return true;
  }
 
 }
 
 Prometheus::processRequest();